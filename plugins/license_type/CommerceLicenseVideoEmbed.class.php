<?php

/**
 * Video Embed license type.
 */
class CommerceLicenseVideoEmbed extends CommerceLicenseBase  {
  /**
   * Implements CommerceLicenseInterface::isConfigurable().
   *
   * Returns whether a license is configurable.
   *
   * Configurable licenses are editable by the customer, either through
   * the add to cart form (via Inline Entity Form) or through a checkout pane.
   *
   * The output of this method determines whether form(), formValidate()
   * and formSubmit() will be called.
   *
   * @return
   *   TRUE if the license is configurable, FALSE otherwise.
   */
  public function isConfigurable() {
    return FALSE;
  }


  /**
   * Implements CommerceLicenseInterface::accessDetails().
   *
   * Returns an html representation of the access details.
   *
   * The information contained within should be the required minimum
   * for the customer to access the resource he purchased the rights to.
   *
   * If the customer purchased a file license, this method would output
   * the link to that file.
   * If the customer purchased a software subscription and the service
   * returned access credentials, this method would return those
   * access credentials.
   *
   * @return
   *   An html string with the access details.
   */
  public function accessDetails() {
    // Add link with colorbox support
    $video_url = $this->wrapper->product->commerce_license_video_embed->video_url->value();
    $link = video_embed_field_get_ajax_url($video_url);
    return l(t('Watch Now'), $link['path'], $link['options']);
  }

  /**
   * Implements CommerceLicenseInterface::checkoutCompletionMessage().
   *
   * Returns the html message to be shown to the customer on checkout complete.
   *
   * Called by the commerce_license_complete checkout pane.
   *
   * @return
   *   The html message to be shown to the customer.
   */
  public function checkoutCompletionMessage() {
    // A real checkoutCompletionMessage() method would also output the result
    // of $this->accessDetails() here.

    $product = $this->wrapper->product->value();
    $message = t('Thank you for purchasing %product.', array('%product' => $product->title)) . '<br />';
    return $message . $this->accessDetails();
  }
}
