<?php

/**
 * @file
 * Provides a license type plugin.
 */

$plugin = array(
  'title' => t('Video Embed'),
  'class' => 'CommerceLicenseVideoEmbed',
  'weight' => 1,
);
