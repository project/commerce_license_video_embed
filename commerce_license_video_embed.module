<?php

/**
 * @file
 * Extends Commerce License with the ability to sell access to video form other sources (Vimeo)
 */

/**
 * Implements hook_menu().
 */
function commerce_license_video_embed_menu() {
  $items['admin/commerce/config/license/video_embed'] = array(
    'title' => 'Video Embed',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('commerce_license_video_embed_settings_form'),
    'access arguments' => array('administer licenses'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'includes/commerce_license_video_embed.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_menu_alter.
 *
 * Add access check for video_embed_field menu item.
 */
function commerce_license_video_embed_menu_alter(&$items) {
  $items['vef/load/%']['access callback'] = 'commerce_license_video_embed_menu_access';
  $items['vef/load/%']['access arguments'] = $items['vef/load/%']['page arguments'];
}

/**
 * Access callback for video_embed_field menu item.
 *
 * @param $hash
 * Video hash used in the menu item.
 */
function commerce_license_video_embed_menu_access($hash) {
  $data = _video_embed_field_load_video($hash);

  if (!isset($data['video_url'])) {
    // No data from the hash, denie access.
    return FALSE;
  }else {
    return commerce_license_video_embed_access($data['video_url']);
  }

}

/**
 * Determines if a user may perform the given operation on the licensed video_url
 *
 * A video_url is licensed if the user has a license for the parent product.
 *
 * Note: When checking access for all embedded video url of a product, it's more
 * performant to simply check whether commerce_license_video_embed_get_product_license()
 * returned a license.
 *
 * @param $hash
 *   The video hash.
 * @param $account
 *   Optional, a user object representing the user for whom the operation is to
 *   be performed. Determines access for a user other than the current user.
 *
 * @return
 *   TRUE if the video url  is not licensable, or the user has access.
 *   FALSE otherwise.
 */
function commerce_license_video_embed_access($video_url, $account=NULL) {
  if (!$account) {
    $account = $GLOBALS['user'];
  }
  // Checkout complete account override for anonymous users.
  if (!empty($_SESSION['commerce_license_video_embed_uid']) && empty($account->uid)) {
    $account = user_load($_SESSION['commerce_license_video_embed_uid']);
  }
  // No need to check access.
  if (commerce_license_video_embed_bypass_license_control($account)) {
    return TRUE;
  }
  if (!commerce_license_video_embed_is_licensable($video_url)) {
    // This file is not licensable. Free access.
    return TRUE;
  }
  watchdog('commerce_license_video_embed', 'license_control', NULL, WATCHDOG_WARNING);

  // Look for a valid license.
  $license = commerce_license_video_embed_get_license($video_url, $account);
  watchdog('commerce_license_video_embed', '<pre>' . print_r( $license, true) . '</pre>', NULL, WATCHDOG_WARNING);
  return !empty($license);
}

/**
 * Checks whether the provided user is allowed to bypass license control.
 *
 * This allows the user to watch a video without having a license.
 *
 * @param $account
 *   The account to check for. If not given, the current user is used instead.
 *
 * @return
 *   TRUE if the account is allowed to bypass license control, FALSE
 *   otherwise.
 */
function commerce_license_video_embed_bypass_license_control($account = NULL) {
  return user_access('bypass license control', $account) || user_access('administer licenses', $account);
}

/**
 * Checks whether the given video url is licensable.
 *
 * A video url is licensable if it's referenced from at least one product's
 * commerce_license_video_embed field.
 *
 * @param $video_url
 *   Video url.
 *
 * @return
 *   TRUE if the video url is licensable, FALSE otherwise.
 */
function commerce_license_video_embed_is_licensable($video_url) {
  $query = new EntityFieldQuery;
  $query
    ->entityCondition('entity_type', 'commerce_product')
    ->fieldCondition('commerce_license_video_embed', 'video_url', $video_url)
    ->count();
  $result = $query->execute();

  return $result > 0;
}

/**
 * Returns an eligible active license for the given video_url
 *
 * A video url could be sold from multiple products. The user's active licenses
 * for all of them are loaded, and the first eligible one is returned.
 *
 * @param $video_url
 *   The video_url string.
 * @param $account
 *   The account to check for. If not given, the current user is used instead.
 *
 * @return
 *   The license if found, FALSE otherwise.
 */
function commerce_license_video_embed_get_license($video_url, $account = NULL) {
  if (!$account) {
    $account = $GLOBALS['user'];
  }
  // Checkout complete account override.
  if (!empty($_SESSION['commerce_license_uid']) && empty($account->uid)) {
    $account = user_load($_SESSION['commerce_license_uid']);
  }
  // Ignore anonymous users, they can't have licenses.
  if (empty($account->uid)) {
    return FALSE;
  }

  $licenses = &drupal_static(__FUNCTION__, array());
  if (!isset($licenses[$video_url])) {
    $licenses[$video_url] = FALSE;

    // Get all products that offer the provided file.
    $query = new EntityFieldQuery;
    $query
      ->entityCondition('entity_type', 'commerce_product')
      ->fieldCondition('commerce_license_video_embed', 'video_url', $video_url);
    $result = $query->execute();
    if (!empty($result['commerce_product'])) {
      $product_ids = array_keys($result['commerce_product']);
      // Get the user's licenses for any of the found product ids.
      // The oldest active licenses are used first.
      $query = new EntityFieldQuery;
      $query
        ->entityCondition('entity_type', 'commerce_license')
        ->entityCondition('bundle', 'video_embed')
        ->propertyCondition('status', COMMERCE_LICENSE_ACTIVE)
        ->propertyCondition('product_id', $product_ids)
        ->propertyCondition('uid', $account->uid)
        ->entityOrderby('entity_id', 'ASC');
      $result = $query->execute();

      if (!empty($result['commerce_license'])) {
        // return first license found.
        $licenses[$video_url] = reset($result['commerce_license']);
      }
    }
  }

  return $licenses[$video_url];
}

/**
 * Implements hook_ctools_plugin_directory().
 */
function commerce_license_video_embed_ctools_plugin_directory($owner, $plugin_type) {
  if ($owner == 'commerce_license') {
    return "plugins/$plugin_type";
  }
}

/**
 * Implements hook_flush_caches().
 *
 * Ensures that products have the required commerce_license_video_embed field.
 */
function commerce_license_video_embed_flush_caches() {
  $product_types = commerce_license_video_embed_product_types();
  commerce_license_video_embed_configure_product_types($product_types);
}

/**
 * Return a list of product types used for file licensing.
 *
 * @return
 *   An array of product type machine names.
 */
function commerce_license_video_embed_product_types() {
  $file_product_types = variable_get('commerce_license_video_embed_product_types', array());
  $file_product_types = array_filter($file_product_types);
  // Return only those $file_product_types that are still licensable.
  $license_product_types = commerce_license_product_types();
  return array_intersect($file_product_types, $license_product_types);
}

/**
 * Ensures that the provided product types have the required fields.
 *
 * Fields:
 * - commerce_license_video_embed: a file field holding the licensable files.
 *
 * @param $types
 *   An array of product type machine names.
 */
function commerce_license_video_embed_configure_product_types($types) {
  $field = field_info_field('commerce_license_video_embed');
  if (!$field) {
    $field = array(
      'field_name' => 'commerce_license_video_embed',
      'cardinality' => 1,
      'type' => 'video_embed_field',
      'entity_types' => array('commerce_product'),
    );
    field_create_field($field);
  }

  $existing = array();
  if (!empty($field['bundles']['commerce_product'])) {
    $existing = $field['bundles']['commerce_product'];
  }
  // Create instances on newly configured product types.
  foreach (array_diff($types, $existing) as $new_bundle) {
    $instance = array(
      'field_name' => 'commerce_license_video_embed',
      'entity_type' => 'commerce_product',
      'bundle' => $new_bundle,
      'label' => t('Embedded Video'),
      'required' => TRUE,
      'settings' => array(
        //'file_extensions' => 'mp4 m4v flv wmv mp3 wav jpg jpeg png pdf doc docx ppt pptx xls xlsx',
        //'description_field' => 1,
      ),
      'widget' => array(
        'type' => 'video_embed_field_video',
      ),
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'commerce_license_video_embed',
          'type' => 'commerce_license_video_embed',
          'settings' => array(),
        ),
      ),
    );
    field_create_instance($instance);
  }
  // Remove instances from product types that can no longer have file licenses.
  foreach (array_diff($existing, $types) as $removed_bundle) {
    $instance = field_info_instance('commerce_product', 'commerce_license_video_embed', $removed_bundle);
    field_delete_instance($instance, TRUE);
  }
}
